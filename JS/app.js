document.getElementById('btnBuscar').addEventListener('click', function() {
    const film = document.getElementById('nombre').value.toLowerCase(); 
    const apiUrl = `https://www.omdbapi.com/?t=${film}&plot=full&apikey=30063268`;

    fetch(apiUrl)
        .then(response => response.json())
        .then(data => {
            if (film == data.Title.toLowerCase()) {
                document.getElementById('resName').innerText = `${data.Title}`;
                document.getElementById('resYear').innerText = `${data.Year}`;
                document.getElementById('resActors').innerText = `${data.Actors}`;
                document.getElementById('resPlot').value = `${data.Plot}`;
                document.getElementById('infoPoster').src = data.Poster;
            } else {
                document.getElementById('resName').innerText = '';
                document.getElementById('resYear').innerText = '';
                document.getElementById('resActors').innerText = '';
                document.getElementById('resPlot').value = '';
                document.getElementById('infoPoster').src = '';
                alert('Ingresa el titulo completo de la película.');
            }
        })
        .catch(error => console.error('Error al obtener datos:', error));
});
